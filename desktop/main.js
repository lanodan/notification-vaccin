const {
    app,
    BrowserWindow,
    ipcMain
} = require('electron')
const path = require('path')
const shell = require('electron').shell;
const Store = require('electron-store');
const store = new Store();

app.setName("Notification Vaccin Covid19");

function createWindow() {
    const win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            preload: path.join(__dirname, 'preload.js')
        },
        icon : "icon.png",
        backgroundColor : "#004141"
    })

    win.loadFile('index.html');
    //win.openDevTools();
    console.log("Window loaded");

    win.webContents.on('new-window', function (e, url) {
        e.preventDefault();
        require('electron').shell.openExternal(url);
    });

}

app.whenReady().then(() => {
    createWindow()

    app.on('activate', () => {
        if (BrowserWindow.getAllWindows().length === 0) {
            createWindow()
        }
    })
})

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

ipcMain.on('pageLoaded', (event, arg) => {
    //acceptedCGU
    console.log("acceptedCGU:"+store.get('acceptedCGU'));
    if(store.get('acceptedCGU') == undefined){
        event.sender.send('acceptedCGU', false);
    }else{
        event.sender.send('acceptedCGU', true);
    }
    //params
    console.log("params:");
    console.log(store.get('params'));
    event.sender.send('params', store.get('params'));
    //saveFav
    console.log("saveFav:")
    console.log(store.get('saveFav'));
    event.sender.send('savedFav', store.get('saveFav'));
});


ipcMain.on('saveFav', (event, arg) => {
    console.log("event saveFav:");
    console.log(
        arg
    );
    store.set({
        'saveFav': arg
    });
});

ipcMain.on('acceptedCGU', (event, arg) => {
    console.log("event acceptedCGU:");
    console.log(
        arg
    );
    store.set({
        'acceptedCGU': true
    });
});


ipcMain.on('saveParams', (event, arg) => {
    console.log("event saveParams:");
    console.log(
        arg
    );
    store.set({
        'params': arg
    });
});

ipcMain.on('trashData', (event, arg) => {
    console.log("event trashData");
    store.clear();
    event.sender.send('trashedData', true);
});