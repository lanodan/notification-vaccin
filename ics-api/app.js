'use strict';

const ical = require('ical-generator');
const express = require('express');
const path = require('path');
const moment = require('moment');
const app = express();
const got = require('got');
const port = 3421;

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '/index.html'));
})

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`)
})

app.get('/24h/:q.ics', async (req, res) => {
    var calendar = ical.default({
        domain: 'notification-vaccin-api.dav.li',
        name: 'Notification vaccin Covid19'
    });
    if (req.params.q) {
        var save = transformInlineToJsonSave(req.params.q);
    }
    for (var dep in save) {
        try {
            const response = await got("https://vitemadose.gitlab.io/vitemadose/" + dep + ".json");
            var result = JSON.parse(response.body);
            if (result && result.centres_disponibles) {
                var depData = result.centres_disponibles.concat(result.centres_indisponibles);
            }
            if (depData) {
                for (var t in save[dep]) {
                    for (var d in depData) {
                        var center = depData[d];
                        if (center.internal_id == save[dep][t]) {
                            var hasAvailability = false;
                            for (var a in center.appointment_schedules) {
                                if (center.appointment_schedules && (center.appointment_schedules[a].name == "1_days" || center.appointment_schedules[a].name == "2_days") && center.appointment_schedules[a].total != 0) {
                                    hasAvailability = true;
                                }
                            }
                            if (hasAvailability) {
                                var start = moment(center.prochain_rdv);
                                var event = calendar.createEvent({
                                    start: start,
                                    end: start.clone().add(1, 'hour'),
                                    summary: "Un rendez-vous vaccinal est disponible !",
                                    description: "Prendre rendez-vous : "+center.url+"\nModifiez vos favoris : https://dav.li/notification-vaccin/#" + transformJsonToInlineSave(save),
                                    location: center.nom,
                                    url: center.url
                                });
                                event.alarms([{
                                    type: 'audio',
                                    trigger: start.diff(moment().add(10, 'seconds'), 'seconds'),
                            }]);
                            }
                        }
                    }
                }
            }
        } catch (error) {

        }
    }
    calendar.serve(res);

});

function transformInlineToJsonSave(inline) {
    var save = {};
    var inlineDep = inline.split("--");
    for (var d in inlineDep) {
        if (inlineDep[d] != "") {
            var inlineLocal = inlineDep[d].split("-");
            var dep = inlineLocal[0];
            if (!save[dep]) {
                save[dep] = [];
            }
            for (var l in inlineLocal) {
                if (l != 0) {
                    save[dep].push(inlineLocal[l]);
                }
            }
        }
    }
    return save;
}

function transformJsonToInlineSave(save) {
    var inline = "";
    for (var s in save) {
        inline += s + "-";
        for (var t in save[s]) {
            inline += save[s][t] + "-";
        }
        inline += "-";
    }
    inline = inline.slice(0, inline.length - 2);
    return inline;
}